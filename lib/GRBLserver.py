from websocket import create_connection
import json
import time
import numpy as np
import operator
import re
import serial


STATUSPAT = re.compile(r"^<(\w*?),MPos:([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),WPos:([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),?(.*)>$")

class grbl:
    def __init__(self, conName="localhost",port=5020,url=''):

            
        self.conn = create_connection("ws://"+conName+":"+str(port)+url+"/ws")
        self.conn.settimeout(1.0)
        self.status=None
        self.destPos=[0.0,0.0,0.0]
        
        if self.conn.recv()=='ConnectionOK':
            print('ConnectionOK')
        
        self.actualizeStatus()
        
    def close(self):
        self.conn.close()
        self.state="undef"
        
    def sendCmd(self,cmd,feedback=False):
        self.conn.send(json.dumps({'data':cmd,'feedback':feedback}))
        if feedback:
            return self.conn.recv()

    def actualizeStatus(self):
        
        self.conn.send('{"srvCmd":"enableStatusNext"}')
        retString=self.conn.recv()
        try:
            self.status=json.loads(retString)
        except ValueError:
            print('cannot decode JSON')
            
                
    def moveTo(self,x=None,y=None,z=None,rel=False):
        cmd="G0"
        if not x is None:
            self.destPos[0]=x;
            cmd=cmd+"X%1.3f"%x;
        if not y is None:
            self.destPos[1]=y;
            cmd=cmd+"Y%1.3f"%y;
        if not z is None:
            self.destPos[2]=z;
            cmd=cmd+"Z%1.3f"%z;
        self.sendCmd(cmd)
        print(cmd)
        
    def homing(self):
        try:
            self.sendCmd('$H')
        except WebSocketTimeoutException:
            pass

    def waitFinish(self):
        
        maxDist=1;
        while self.status['State']!="Idle" or maxDist>0.01:
            time.sleep(0.25)
            self.actualizeStatus()
            #maxDist=0
            #print(self.status['MPos'])
            #print(self.destPos)
            maxDist= np.max(np.absolute((self.status['MPos'][0]-self.destPos[0],self.status['MPos'][1]-self.destPos[1],self.status['MPos'][2]-self.destPos[2])))
            #print(maxDist)
    




