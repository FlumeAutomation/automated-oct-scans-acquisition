// OctControl.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
//

#include "stdafx.h"	 // standard

#include <iostream>
#include <fstream>
#include <Windows.h>
#include "SpectralRadar.h"	//library Thorlabs

#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string

#ifdef _DEBUG
#undef _DEBUG
#include "Python.h"
#define _DEBUG
#endif

using namespace std;

PyObject* parameters = PyDict_New();

OCTDeviceHandle Dev;	// define OCTDeviceHandle as Dev 
ProbeHandle Probe;
ProcessingHandle Proc;



// create the directory
int createDirectory(string path) {		// path specified in .ini
	std::wstring ws = std::wstring(path.begin(), path.end());
	if (!CreateDirectory(ws.c_str(), NULL)) {
		string error_Str;
		switch (GetLastError()) {
		case ERROR_ALREADY_EXISTS:
			error_Str += "Error directory '" + path + "' already exists";
			PyErr_SetString(PyExc_RuntimeError, error_Str.c_str());
			return -1;
		case ERROR_PATH_NOT_FOUND:
			error_Str += "Error directory '" + path + "' ERROR_PATH_NOT_FOUND";
			PyErr_SetString(PyExc_RuntimeError, error_Str.c_str());
			return -1;
		}
	}
	return 0;
}


// initialization of the oct
static PyObject* init(PyObject * self, PyObject *args, PyObject *keywds) {
	Dev = initDevice();
	cout << "initProbe" << endl;
	Probe = initProbe(Dev, "ProbeLSM03");
	cout << "createProcessingForDevice" << endl;
	Proc = createProcessingForDevice(Dev);
	Py_RETURN_NONE;
}

//RAWdata is object of concatenated images ()
//convert images into 8 bit
void exportRAWGrayscale8bit(float * RAWdata,char * path, uint64_t numPixel,float level,float window) {
	char * buffer;
	uint64_t outputSize = numPixel * sizeof(char);

	cout << "numPixel: " << numPixel << endl;
	cout << "outputSize: " << outputSize << endl;
	buffer = (char*)malloc(outputSize);
	ofstream myFile(path, ios::out | ios::binary);
	float minVal = level - window / 2;	//level and window def in .ini
	float maxVal = level + window / 2;
	uint64_t i;
	//cout << "ici: " << endl;
	for (i = 0; i < numPixel; i++) {
		if (RAWdata[i] < minVal) {
			buffer[i] = 0;
		}
		else if (RAWdata[i] > maxVal) {
			buffer[i] = 255;
		}
		else {
			buffer[i] =(int) ((RAWdata[i]-minVal)/(window)*255.0);
		}
		//cout << "pixel " << i << "val " << RAWdata[i] << "out " << (int)buffer[i] << endl;
	}
	//cout << "la: " << endl;
	myFile.write(buffer, outputSize);
	myFile.close();
}

void exportRAWGrayscale(float * RAWdata, char * path, uint64_t numPixel) {
	uint64_t outputSize = numPixel * sizeof(float);
	ofstream myFile(path, ios::out | ios::binary);
	myFile.write((char *)RAWdata, outputSize);
	myFile.close();
}


//
string getStringFromParams(PyObject * localParams,const char * varaibleName) {
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	std::stringstream  path_str;
	PyObject * param = PyDict_GetItemString(localParams, varaibleName);
	if (param == NULL) {
		cout << "cannot find " << varaibleName << endl;
		return string();
	}
	path_str << (std::put_time(std::localtime(&in_time_t), PyUnicode_AsUTF8(param)));

	cout << "found " << varaibleName << path_str.str() << endl;
	return path_str.str();
}

//
static PyObject* scan(PyObject * self, PyObject *args, PyObject *keywds) {

	//----------- Get The parameters -----------------
	PyObject * localParams= PyDict_Copy(parameters);
	if (keywds != NULL) {
		if (PyDict_Merge(localParams, keywds, 1)) {
			cout << "Error while setting parameters " << endl;
			return NULL;
		}
	}
	int size_z = getDevicePropertyInt(Dev, DevicePropertyInt::Device_BytesPerElement);	//here evice_SpectrumElements in FiniteStackMeasurements
	int size_x = PyLong_AsLong(PyDict_GetItemString(localParams, "nAscans"));
	int size_y = PyLong_AsLong(PyDict_GetItemString(localParams, "nBscans"));
	int avg_Ascans = PyLong_AsLong(PyDict_GetItemString(localParams, "avg_Ascans"));
//	int avg_Ascans = PyLong_AsLong(PyDict_GetItemString(localParams, "avg_Ascans"));
	int oversampling_y = 1;
	
	double x = PyFloat_AsDouble(PyDict_GetItemString(localParams, "X"));
	double y = PyFloat_AsDouble(PyDict_GetItemString(localParams, "Y"));
	string path = getStringFromParams(localParams, "path");
	if (createDirectory(path)) { return NULL; }
	PyDict_SetItemString(localParams, "path_real", PyUnicode_FromString(path.c_str()));

	cout << "Aqui" << endl;
	// string defines a new xx in the dictionary in python
	//Get the color Boundaries
	float colorBoundaries[] = { -20.0,80.0 };
	PyObject * pyColorBoundaries = PyDict_GetItemString(localParams, "colorBoundaries");
	if (pyColorBoundaries != NULL) {
		colorBoundaries[0] = (float)PyFloat_AsDouble(PyList_GetItem(pyColorBoundaries, 0));
		colorBoundaries[1] = (float)PyFloat_AsDouble(PyList_GetItem(pyColorBoundaries, 1));
	}

	string RAWGrayscale8bitFile = getStringFromParams(localParams, "exportRAWGrayscale8bit");
	string RAWGrayscalFile = getStringFromParams(localParams, "exportRAWGrayscale");
	string TiffFilePathName = getStringFromParams(localParams, "exportTIFF");
	string SRMFile = getStringFromParams(localParams, "exportSRM");

	string TiffFilePath=string();
	if(!TiffFilePathName.empty()){
		TiffFilePath = path +"\\"+ TiffFilePathName;
		if (createDirectory(TiffFilePath)) { return NULL; }
	}

	//----------- Configure the OCT -----------------
	setProbeParameterInt(Probe, Probe_Oversampling, 1);
	setProcessingParameterInt(Proc, Processing_AScanAveraging, 3);
//	setProcessingParameterInt(Proc, Processing_SpectrumAveraging, avg_Ascans);
	setProbeParameterInt(Probe, Probe_Oversampling_SlowAxis, oversampling_y);
//	ScanPatternHandle Pattern = createBScanStackPattern(Probe, x, size_x*avg_Ascans, y, size_y); //collect the A scans  into B scans
	setProcessingAveragingAlgorithm(Proc, Processing_Averaging_Fourier_Max);
	ScanPatternHandle Pattern = createBScanStackPattern(Probe, x, size_x*3, y, size_y); //collect the A scans  into B scans
	DataHandle BScan = createData();
	RawDataHandle Raw = createRawData();

	ColoredDataHandle ColBScan;
	Coloring32BitHandle Color;
	if (!TiffFilePath.empty()) {
		ColBScan = createColoredData();
		Color = createColoring32Bit(ColorScheme_BlackAndWhite, Coloring_RGBA);
		setColoringBoundaries(Color, colorBoundaries[0], colorBoundaries[1]);
	}

	int numPixelAscan = size_x * 1024;
	uint64_t numPixelTotal = size_y * numPixelAscan;
	float* RAWbuffer = (float*)malloc(numPixelTotal *sizeof(float));	//malloc is a memory reservation for the whole 3d image (1Gb)
	
	startMeasurement(Dev, Pattern, Acquisition_AsyncFinite);

	char outputFile[200];
	for (int i = 0; i < size_y*oversampling_y; ++i)  //loop that collects the B scans
	{
		clock_t start = clock();
		getRawData(Dev, Raw);
		setProcessedDataOutput(Proc, BScan);
		if (!TiffFilePath.empty()) {
			setColoredDataOutput(Proc, ColBScan, Color);
		}
		executeProcessing(Proc, Raw);

		cout << "Current image " << i << "/" << size_y << " ";
		if (!TiffFilePath.empty()) {
			sprintf_s(outputFile, "%s\\tiff_%04d", TiffFilePath.c_str(), i);
			exportColoredData(ColBScan, ColoredDataExport_TIFF, outputFile);
		}

		memcpy(&RAWbuffer[i*numPixelAscan], getDataPtr(BScan), numPixelAscan * sizeof(float));  //takes one B scans and put it into reserved memory
		clock_t stop = clock();
		cout << "Time used: " << static_cast<double>(stop - start) / CLOCKS_PER_SEC << '\r'; //cout is print function
		start = stop;
	}
	cout << "BScan Size: " << getDataPropertyFloat(BScan, Data_Range1) << " x " << getDataPropertyFloat(BScan, Data_Range2) << "  ";
	cout << "BScan Pixels Size: " << getDataPropertyInt(BScan, Data_Size1) << " x " << getDataPropertyInt(BScan, Data_Size2) << " x " << getDataPropertyInt(BScan, Data_Size3) << "  ";
	
	//----------- Set the Returned Values -----------------

	

	// Set the imgShape Variable list
	int imgShape[] = { size_y,getDataPropertyInt(BScan, Data_Size2), getDataPropertyInt(BScan, Data_Size1) };
	PyObject * pyImgShape = PyList_New(3);
	for (int i = 0; i < 3; i++) {
		PyObject * jpo = PyLong_FromLong(imgShape[i]);
		PyList_SetItem(pyImgShape, i, jpo);
	}
	PyDict_SetItemString(localParams, "imgShape", pyImgShape);

	// Set the imgRange Variable list
	double imgRangeEnd[] = { y, x,getDataPropertyFloat(BScan, Data_Range1) };
	PyObject * pyImgRange = PyList_New(3);
	for (int i = 0; i < 3; i++) {
		PyObject * pyImgRangeDim = PyList_New(2);
		PyObject * jpo0 = PyFloat_FromDouble(0.0);
		PyObject * jpo1 = PyFloat_FromDouble(imgRangeEnd[i]);
		PyList_SetItem(pyImgRangeDim, 0, jpo0);
		PyList_SetItem(pyImgRangeDim, 1, jpo1);
		PyList_SetItem(pyImgRange, i, pyImgRangeDim);
	}
	PyDict_SetItemString(localParams, "imgRange", pyImgRange);

	// Set Axis direction
	PyObject * pyAxisDir = PyList_New(3);
	PyList_SetItem(pyAxisDir, 0, PyUnicode_FromString("x"));
	PyList_SetItem(pyAxisDir, 1, PyUnicode_FromString("y"));
	PyList_SetItem(pyAxisDir, 2, PyUnicode_FromString("Z-"));
	PyDict_SetItemString(localParams, "axisDir", pyAxisDir);

	// ------------- Export the Data ------------
	if (!SRMFile.empty()) {
		sprintf_s(outputFile, "%s\\%s.srm", path.c_str(), SRMFile.c_str());
		cout << "Export SRMFile '" << outputFile << "'" << endl;
		exportData2D(BScan, Data2DExport_SRM, outputFile);
		PyDict_SetItemString(localParams, "path_srm", PyUnicode_FromString(outputFile));
	}
	if (!RAWGrayscale8bitFile.empty()) {
		sprintf_s(outputFile, "%s\\%s.raw", path.c_str(), RAWGrayscale8bitFile.c_str());
		cout << "Export RAWGrayscale8bitFile '" << outputFile << "'" << endl;
		exportRAWGrayscale8bit(RAWbuffer, outputFile, numPixelTotal, colorBoundaries[0], colorBoundaries[1]);
		PyDict_SetItemString(localParams, "path_raw8bit", PyUnicode_FromString(outputFile));
	}
	if (!RAWGrayscalFile.empty()) {
		sprintf_s(outputFile, "%s\\%s.raw", path.c_str(), RAWGrayscalFile.c_str());
		cout << "Export RAWGrayscalFile '" << outputFile << "'" << endl;
		exportRAWGrayscale(RAWbuffer, outputFile, numPixelTotal);
		PyDict_SetItemString(localParams, "path_rawfloat", PyUnicode_FromString(outputFile));
	}
	
	free(RAWbuffer);

	stopMeasurement(Dev);
	clearRawData(Raw);
	clearData(BScan);
	clearScanPattern(Pattern);
	return localParams;
}

static PyObject* close_oct(PyObject * self, PyObject *args, PyObject *keywds) {
	closeProbe(Probe);
	closeProcessing(Proc);
	closeDevice(Dev);
	Py_RETURN_NONE;
}

static PyObject* setParameters(PyObject * self, PyObject *args, PyObject *keywds) {
	if (PyDict_Merge(parameters, keywds, 1)) {
		cout << "Error while setting parameters " <<  endl;
		return NULL;
	}
	Py_RETURN_NONE;
}

static PyObject* sayHello(PyObject * self, PyObject *args, PyObject *keywds)
{
	float f[] = {-40, 20, 60, 100};
	//exportRAWGrayscale8bit(f, 4, -40.0f, 50.0f);

	

	Py_RETURN_NONE;
}


//all functions that you can call from python
static PyMethodDef OCT_CONTROL_METHODS[] =
{
	{ "sayHello",(PyCFunction)sayHello, METH_VARARGS|METH_KEYWORDS, "A simple example of an embedded function." },
	{ "setParameters",(PyCFunction)setParameters, METH_VARARGS | METH_KEYWORDS, "Set the default parameters of the OCT" },
	{ "init",(PyCFunction)init, METH_VARARGS | METH_KEYWORDS, "initialize OCT" },
	{ "scan",(PyCFunction)scan, METH_VARARGS | METH_KEYWORDS, "scan OCT" },
	{ "close",(PyCFunction)close_oct, METH_VARARGS | METH_KEYWORDS, "exit OCT" },
	NULL
};

static struct PyModuleDef OCT_CONTROL_MODULE = {
	PyModuleDef_HEAD_INIT,
	"OctControl",
	NULL,
	-1,
	OCT_CONTROL_METHODS,
	NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC
PyInit_OctControl(void)
{
	std::cout << "Initializaing module!" << std::endl;
	PyObject* m = PyModule_Create(&OCT_CONTROL_MODULE);
	if (!m)
	{
		std::cerr << "Failed to create module..." << std::endl;
		return NULL;
	}
	return m;
}

